# Integratieproject 1 - 2019/2020

Front end applicatie geschreven met het Vue.js framework voor de educatieve stemtest.  
Een project door **Centennials**.

## Beschrijving

De front end van de educatieve stemtest is een webapplicatie geschreven met het moderne JavaScript framework [Vue.js](https://vuejs.org/). Het doel van deze applicatie is om leerlingen de mogelijkheid te geven om via de browser te kunnen deelnemen aan een spel opgestart door de leerkracht. Ook wordt het voornamelijk gebruikt door leerkrachten voor hun testen te beheren en lessen te starten. Voor super admins wordt het platform gebruikt om scholen te beheren en algemene gebruiksstatistieken te bekijken.

## Contributors / Leden

Hieronder is een overzicht van alle leden die hebben deelgenomen aan dit project. Onderstaande lijst is alfabetisch gesorteerd op achternaam.

| Achternaam | Voornaam |
| ---------- | -------- |
| Anthonisen | Bert     |
| El Nasiri  | Issam    |
| Goemans    | Gerd     |
| Koyen      | Joren    |
| Maene      | Kevin    |
| Vroemans   | Anton    |

## Software vereisten

> Volgende software is nodig om de applicatie te kunnen _builden_ en _runnen_.

- [npm](https://www.npmjs.com/) · v6.x.x
- [node.js](https://nodejs.org/en/) · v12.x.x

## Instructies

> Clone de repository naar een gewenste locatie met behulp van de ssh url of http url.

```bash
git clone git@gitlab.com:kdg-ti/integratieproject-1/centennials/front-end.git
```

```bash
git clone https://gitlab.com/kdg-ti/integratieproject-1/centennials/front-end.git
```

---

> Navigeer naar de directory van de repository vooraleer volgende commando's uit te voeren.

```bash
# IMPORTANT: installeer alle dependencies
npm install

# start development server
npm run dev
```
