// eslint-disable-next-line no-unused-vars
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';
import store from './store';
import { sleep } from './util/sleep';

export default {
  install(Vue) {
    const sessionHub = new Vue();
    Vue.prototype.$sessionHub = sessionHub;

    /**
     * @type HubConnection
     */
    let connection = null;
    let tryRestart = true;
    let loader = null;

    let connectionOpenedCallbacks = [];

    Vue.prototype.startSignalR = () => {
      connection = new HubConnectionBuilder()
        .withUrl(process.env.VUE_APP_SESSION_HUB_URL, {
          accessTokenFactory: () => store.state.token
        })
        .configureLogging(process.env.VUE_APP_SIGNALR_LOGLEVEL)
        .build();

      ////////////////////
      //receiving events//
      ////////////////////
      connection.on('SessionJoined', session => {
        sessionHub.$emit('SessionJoined', { session });
      });
      connection.on('ServeQuestion', question => {
        sessionHub.$emit('ServeQuestion', { question });
      });
      connection.on('AnswerReceived', answer => {
        sessionHub.$emit('AnswerReceived', { answer });
      });
      connection.on('SessionCompleted', () => {
        sessionHub.$emit('SessionCompleted');
      });
      connection.on('OpenSessionResult', session => {
        sessionHub.$emit('OpenSessionResult', { session });
      });
      connection.on('SessionStarted', session => {
        sessionHub.$emit('SessionStarted', { session });
      });
      connection.on('SessionClosed', session => {
        sessionHub.$emit('SessionClosed', { session });
      });
      connection.on('CurrentQuestion', question => {
        sessionHub.$emit('CurrentQuestion', { question });
      });
      connection.on('StudentConnected', numberOfStudentsConnected => {
        console.log('StudentConnected', numberOfStudentsConnected);
        sessionHub.$emit('StudentConnected', { numberOfStudentsConnected });
      });
      connection.on('NumberOfMembersNotReady', NumberOfMembersNotReady => {
        sessionHub.$emit('NumberOfMembersNotReady', {
          NumberOfMembersNotReady
        });
      });

      //////////////////
      //Sending events//
      //////////////////
      sessionHub.ConnectionOpen = fun => {
        if (sessionHub.IsConnected) {
          return fun();
        }
        connectionOpenedCallbacks.push(fun);
      };

      sessionHub.JoinSession = sessionId => {
        return connection.invoke('JoinSession', sessionId);
      };
      sessionHub.SelectParty = (opaque, partyId) => {
        return connection.invoke('SelectParty', opaque, partyId);
      };
      sessionHub.Answer = (opaque, questionId, questionIndex, answerId, comment) => {
        return connection.invoke(
          'Answer',
          opaque,
          questionId,
          questionIndex,
          answerId,
          comment
        );
      };

      sessionHub.OpenSession = sessionId => {
        return connection.invoke('OpenSession', sessionId);
      };
      sessionHub.ActivateSession = sessionId => {
        return connection.invoke('StartSession', sessionId, false);
      };
      sessionHub.ForceActivateSession = sessionId => {
        return connection.invoke('StartSession', sessionId, true);
      };
      sessionHub.NextQuestion = (sessionId, index) =>
        connection.invoke('NextQuestion', sessionId, index);
      sessionHub.EndSession = sessionId => connection.invoke('EndSession', sessionId);
      sessionHub.GetCurrentQuestion = pin => connection.invoke('GetCurrentQuestion', pin);

      sessionHub.IsConnected = false;

      /**
       * Starts the signalR connection
       */
      const start = async () => {
        // start connection
        await connection.start();

        // update session hub state
        sessionHub.IsConnected = true;

        // invoke all callbacks
        connectionOpenedCallbacks.forEach(fn => fn());
      };

      /**
       * Tries to connect to the signalR hub
       */
      const tryStart = async () => {
        let thrownOnce = false;
        while (!sessionHub.IsConnected) {
          try {
            await start();
          } catch (err) {
            thrownOnce = true;
            console.error('failed to connect, retrying in 5000ms');
            await sleep(5000);
          }
        }

        if (loader) {
          Vue.$vToastify.stopLoader(loader);
        }

        if (thrownOnce) {
          Vue.$vToastify.success('Reconnected with SignalR service');
        }
      };

      connection.onclose(() => {
        console.warn('signalR connection was closed');
        sessionHub.IsConnected = false;
        if (tryRestart) {
          loader = Vue.$vToastify.loader('Trying to reconnect with SignalR');
          tryStart();
        }
      });

      // Start everything
      tryStart();
    };

    /**
     * Stops the signalR instance
     */
    Vue.prototype.stopSignalR = (restart = false) => {
      if (!connection) return;
      if (restart) tryRestart = true;
      else tryRestart = false;

      return connection.stop();
    };

    Vue.prototype.startSignalR();
  }
};
