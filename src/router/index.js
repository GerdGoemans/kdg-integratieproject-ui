import Vue from 'vue';
import VueRouter from 'vue-router';
import { routes } from './routes';
import store from '../store';
import axios from 'axios';

// needed for router-view
Vue.use(VueRouter);

const router = new VueRouter({
  routes,
  mode: 'history',
  linkExactActiveClass: 'is-active'
});

router.beforeEach(async (to, from, next) => {
  // check if page requires auth and user is not logged in
  if ((!store.getters.isLoggedIn || !store.getters.getToken) && to.meta.requiresAuth) {
    return next('/login');
  }

  // set default authorization header
  const token = store.getters.getToken;
  if (!token && to.meta.requiresAuth) {
    return next('/login');
  } else if (!token) {
    return next();
  }

  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  store.dispatch('headerSet');

  // check if we collected user data
  const hasUser = Object.keys(store.getters.getUser).length > 0;
  if (!hasUser) await store.dispatch('fetchLoginData');

  // redirect to dashboard if we are already logged in
  if (to.name === 'Login') return next('/dashboard');

  next();
});

// Change title middleware
router.beforeEach((to, from, next) => {
  document.title = `${process.env.VUE_APP_DISPLAY_NAME} | ${to.meta.title || '404'}`;
  next();
});

import UsageDataService from '../services/usageDataService';

router.afterEach((to, from) => {
  let fromPage = '';
  let toPage = '';

  if (from.matched.length > 0) {
    let tempFrom = from.matched[0].path;
    if (tempFrom == '') {
      fromPage = '/home';
    } else if (tempFrom == '*') {
      fromPage = '/404';
    } else {
      fromPage = tempFrom;
    }
  }
  if (to.matched.length > 0) {
    let temTo = to.matched[0].path;
    if (temTo == '') {
      toPage = '/home';
    } else if (temTo == '*') {
      toPage = '/404';
    } else {
      toPage = temTo;
    }
  }
  let role = 0;
  if (store.getters.getUser.role) role = store.getters.getUser.role;
  UsageDataService.postRoute(fromPage, toPage, role);
});

export default router;
