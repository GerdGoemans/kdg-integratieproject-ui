const routes = [
  {
    path: '/',
    component: () => import('../pages/join.page'),
    alias: ['/home'],
    meta: {
      title: 'Deelnemen',
      layout: 'default'
    }
  },
  {
    path: '/votetests',
    component: () => import('../pages/voteTestsOverview.page'),
    alias: ['/stemtesten'],
    meta: {
      title: 'Stemtest',
      layout: 'default'
    }
  },
  {
    path: '/faq',
    name: 'faq',
    component: () => import('../pages/faq.page'),
    query: true,
    meta: {
      title: 'FAQ',
      layout: 'default'
    }
  },
  {
    path: '/game/:gameId',
    component: () => import('../pages/game.page'),
    props: true,
    meta: {
      title: 'Game',
      layout: 'default'
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../pages/login.page'),
    props: true,
    meta: {
      title: 'Login',
      layout: 'default'
    }
  },
  {
    path: '/forgot/:resetkey',
    name: 'PassRequestWithKey',
    component: () => import('../pages/forgot.page'),
    props: true,
    meta: {
      title: 'PassRequest',
      layout: 'default'
    }
  },
  {
    path: '/forgot',
    name: 'PassRequest',
    component: () => import('../pages/forgot.page'),
    meta: {
      title: 'PassRequest',
      layout: 'default'
    }
  },
  {
    path: '/dashboard',
    component: () => import('../pages/dashboard.page'),
    props: true,
    meta: {
      title: 'Dashboard',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    path: '/profile',
    component: () => import('../pages/profile.page'),
    props: true,
    meta: {
      title: 'Profile',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    path: '/courses',
    component: () => import('../pages/courses.page'),
    props: true,
    meta: {
      title: 'Courses',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    path: '/editcourse',
    name: 'editCourse',
    component: () => import('../pages/editCourse.page'),
    props: true,
    meta: {
      title: 'Edit Course',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    path: '/createCourse',
    name: 'createCourse',
    component: () => import('../pages/createCourse.page'),
    props: true,
    meta: {
      title: 'Add Course',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    path: '/discover/tests',
    component: () => import('../pages/ontdek.page'),
    props: true,
    meta: {
      title: 'Ontdek',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'privacy',
    props: true,
    path: '/privacy/:returnPage',
    component: () => import('../pages/privacy.page'),
    meta: {
      title: 'privacy',
      layout: 'default'
    }
  },
  {
    name: 'cookie',
    props: true,
    path: '/cookies',
    component: () => import('../pages/cookieInfo.page'),
    meta: {
      title: 'cookie',
      layout: 'default'
    }
  },
  {
    name: 'CreateTest',
    props: true,
    path: '/tests/create',
    component: () => import('../pages/createTest.page'),
    meta: {
      title: 'Create Test',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'EditTest',
    props: true,
    path: '/tests/edit/:testId',
    component: () => import('../pages/editTest.page'),
    meta: {
      title: 'Edit Test',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'MyTests',
    props: true,
    path: '/tests',
    component: () => import('../pages/myTests.page'),
    meta: {
      title: 'Tests',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'ImportTest',
    path: '/tests/import',
    component: () => import('../pages/importTest.page.vue'),
    meta: {
      title: 'Import Test',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'Popular Tests',
    props: true,
    path: '/discover/popular',
    component: () => import('../pages/popularTests.page'),
    meta: {
      title: 'Tests',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'MySessions',
    props: true,
    path: '/lessons',
    component: () => import('../pages/mySessions.page'),
    meta: {
      title: 'Lessons',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'CreateLessons',
    props: { redirectFromSessionsPage: true },
    path: '/lessons/create',
    component: () => import('../pages/myTests.page'),
    meta: {
      title: 'Lessons',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'MyFavouriteTests',
    props: { onlyFavourites: true },
    path: '/tests/favourite',
    component: () => import('../pages/myTests.page'),
    meta: {
      title: 'Tests',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'Schools',
    props: true,
    path: '/schools',
    component: () => import('../pages/schools.page'),
    meta: {
      title: 'Schools',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'Create schools',
    props: true,
    path: '/school/create',
    component: () => import('../pages/createSchool.page'),
    meta: {
      title: 'Create School',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'Parties',
    props: true,
    path: '/parties',
    component: () => import('../pages/manageParties.page'),
    meta: {
      title: 'Manage parties',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'Tags',
    props: true,
    path: '/tags',
    component: () => import('../pages/manageTags.page'),
    meta: {
      title: 'Manage tags',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'Manage Teachers',
    props: true,
    path: '/manageTeachers',
    component: () => import('../pages/manageTeachers.page'),
    meta: {
      title: 'Manage Teachers',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'Edit FAQ',
    props: true,
    path: '/faq/edit',
    component: () => import('../pages/faqEdit.page'),
    meta: {
      title: 'Edit FAQ',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'PrepareSession',
    props: true,
    path: '/prepare/test/:testId',
    component: () => import('../pages/prepareSession.page'),
    meta: {
      title: 'Test',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'Session',
    props: true,
    path: '/session/:sessionId',
    component: () => import('../pages/session.page'),
    meta: {
      title: 'Session',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'SessionResult',
    props: true,
    path: '/session/:sessionId/results',
    component: () => import('../pages/session.result.page'),
    meta: {
      title: 'Session result',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'UsageData',
    props: true,
    path: '/usage',
    component: () => import('../pages/usageData.page'),
    meta: {
      title: 'Usage Data',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'Create Super Admins',
    props: true,
    path: '/superadmin/create',
    component: () => import('../pages/createSuperAdmin.page'),
    meta: {
      title: 'Create Super Admin',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: 'Create Vote test',
    props: true,
    path: '/voteTest',
    component: () => import('../pages/manageVoteTests.page'),
    meta: {
      title: 'Create Vote test',
      layout: 'main',
      requiresAuth: true
    }
  },
  {
    name: '404 not found',
    props: true,
    path: '/404',
    component: () => import('../pages/not-found.page'),
    meta: {
      title: '404',
      layout: 'default',
      requiresAuth: false
    }
  }
];

// push not found route
routes.push({
  path: '*',
  alias: ['/404'],
  component: () => import('../pages/not-found.page'),
  meta: {
    title: '404',
    layout: 'default'
  }
});

export { routes };
