export default [
  {
    label: 'sidebar.general',
    items: [
      {
        path: '/dashboard',
        label: 'sidebar.dashboard'
      },
      {
        path: '/faq/edit',
        label: 'sidebar.superadmin.faq'
      },
      {
        path: '/tags',
        label: 'sidebar.superadmin.tags'
      },
      {
        path: '/usage',
        label: 'sidebar.superadmin.usage'
      }
    ]
  },
  {
    label: 'sidebar.superadmin.schools',
    items: [
      {
        path: '/schools',
        label: 'sidebar.superadmin.schools'
      }
    ]
  },
  {
    label: 'sidebar.superadmin.voteTestHeader',
    items: [
      {
        path: '/parties',
        label: 'sidebar.superadmin.parties'
      },
      {
        path: '/voteTest',
        label: 'sidebar.superadmin.voteTest'
      }
    ]
  },
  {
    label: 'sidebar.superAdmins',
    items: [
      {
        path: '/superadmin/create',
        label: 'sidebar.superadmin.createSA'
      }
    ]
  }
];
