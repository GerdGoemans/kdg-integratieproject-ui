export default [
  {
    label: 'sidebar.general',
    items: [
      {
        path: '/dashboard',
        label: 'sidebar.dashboard'
      },
      {
        path: '/courses',
        label: 'sidebar.teacher.courses'
      },
      {
        path: '/tests',
        label: 'sidebar.teacher.tests',
        items: [
          {
            path: '',
            label: 'sidebar.teacher.myTests'
          },
          {
            path: 'favourite',
            label: 'sidebar.teacher.favouriteTests'
          }
        ]
      },
      {
        path: '/lessons',
        label: 'sidebar.teacher.lessons',
        items: [
          {
            path: '',
            label: 'sidebar.teacher.myLessons'
          }
        ]
      }
    ]
  },
  {
    label: 'sidebar.school.manage',
    items: [
      {
        path: '/manageTeachers',
        label: 'sidebar.school.manageTeachers'
      }
    ]
  },
  {
    label: 'sidebar.explore',
    items: [
      {
        path: '/discover/tests',
        label: 'sidebar.teacher.discoverTests'
      },
      {
        path: '/discover/popular',
        label: 'sidebar.teacher.popularTests'
      }
    ]
  }
];
