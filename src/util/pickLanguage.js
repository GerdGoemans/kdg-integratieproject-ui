export default function pickLanguage(context) {
  let inputOptions = {};
  context.$i18n.availableLocales.forEach(key => {
    let str = context.$i18n.t('languages.' + key);
    inputOptions[key] = str;
  });

  context
    .$fire({
      title: context.$i18n.t('selectLanguage.title'),
      inputPlaceholder: context.$i18n.t('selectLanguage.title'),
      input: 'select',
      inputOptions: inputOptions,
      showCancelButton: true,
      type: 'question'
    })
    .then(language => {
      if (language.dismiss) {
        return;
      }
      context.$i18n.locale = language.value;
      context.$cookies.set('language', context.$i18n.locale, -1);
    });
}
