import TestService from '../services/testService';

export default async function changePrivacy(context, privOptions, testId) {
  context
    .$fire({
      title: context.$i18n.t('selectVisibility.title'),
      inputPlaceholder: context.$i18n.t('selectVisibility.placeholder'),
      input: 'select',
      inputOptions: privOptions,
      showCancelButton: true,
      type: 'question'
    })
    .then(async function(visibility) {
      if (visibility.dismiss) {
        return;
      }
      try {
        await TestService.updateVisibilityTest(testId, parseInt(visibility.value) + 1);

        context.$fire({
          title: context.$i18n.t('success'),
          text: context.$i18n.t('selectVisibility.success'),
          type: 'success',
          timer: 3000
        });

        context.getTests(1);
      } catch (e) {
        context.$fire({
          title: context.$i18n.t('somethingWentWrong'),
          html: context.$i18n.t('selectVisibility.error'),
          showCloseButton: true,
          showCancelButton: false,
          focusConfirm: true,
          confirmButtonText: context.$i18n.t('ok'),
          type: 'error'
        });
      }
    });
}
