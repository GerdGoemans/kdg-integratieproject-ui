import axios from 'axios';
const URL = process.env.VUE_APP_API_URL + '/sessions';

export default class SessionService {
  static async createSession(testId, sessionDto) {
    return axios.post(URL + '/create/' + testId, sessionDto).then(response => {
      return response.data;
    });
  }
  static async getSessionPinOfVoteTest(testId) {
    return axios.get(URL + '/' + testId + '/votetest').then(response => response.data);
  }
  static async getSessionByPin(pin) {
    return axios.get(URL + '/' + pin).then(response => response.data);
  }
  static async getSessionById(id) {
    return axios.get(URL + '/id/' + id).then(response => response.data);
  }
  static async getEndSessionData(id) {
    return axios.get(URL + '/' + id + '/results').then(response => response.data);
  }
  static async getSessionsByUserId(page, query, onlyFromClass = '') {
    let finalUrl = `${URL}?page=${page}&query=${query}&classId=${onlyFromClass}`;
    return axios.get(finalUrl).then(response => response);
  }
  static async closeSession(sessionId) {
    return axios.patch(URL + '/' + sessionId + '/close').then(response => response);
  }
  static async getLatestSessions() {
    return axios.get(URL + '/latest').then(response => {
      return response.data;
    });
  }

  static async getResultsSession(opaque) {
    return axios.get(URL + '/results/' + opaque).then(response => {
      return response.data;
    });
  }

  static async getParticipationData(sessionId) {
    return axios.get(URL + '/' + sessionId + '/results/participation').then(response => {
      return response.data;
    });
  }
}
