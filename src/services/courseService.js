import axios from 'axios';
const URL = process.env.VUE_APP_API_URL + '/courses';

export default class CourseService {
  static async getCourses() {
    return axios.get(URL).then(response => {
      return response.data;
    });
  }

  static async getLatestCourses() {
    return axios.get(URL + '/latest').then(response => {
      return response.data;
    });
  }

  static async updateCourse(object) {
    return axios.put(URL, object).then(response => {
      return response.data;
    });
  }

  static async addTestToCourse(courseId, testId) {
    return axios.put(`${URL}/${courseId}/tests/${testId}`).then(response => {
      return response.data;
    });
  }

  static async deleteTestFromCourse(courseId, testId) {
    return axios.delete(`${URL}/${courseId}/tests/${testId}`).then(response => {
      return response.data;
    });
  }

  static async getCoursesWithTestId(testId) {
    return axios.get(URL + '/courses/test/' + testId).then(response => {
      return response.data;
    });
  }

  static async addCourse(object) {
    return axios.post(URL, object).then(response => {
      return response.data;
    });
  }

  static async deleteCourse(object) {
    return axios
      .delete(URL, {
        data: {
          Id: object.id
        }
      })
      .then(response => {
        return response.data;
      });
  }
}
