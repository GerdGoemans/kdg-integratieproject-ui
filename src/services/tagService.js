import axios from 'axios';

const URL = process.env.VUE_APP_API_URL + '/tags';

export default class TagService {
  static async getTags() {
    return axios.get(URL).then(response => response);
  }
  static async updateTag(tagId, tag) {
    return axios.patch(URL + '/' + tagId, tag).then(response => response);
  }
  static async createTag(tag) {
    return axios.put(URL, tag).then(response => response);
  }
}
