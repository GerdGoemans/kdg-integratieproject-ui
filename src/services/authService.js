import axios from 'axios';

const URL = process.env.VUE_APP_AUTH_URL;

export default class AuthService {
  static login({ email, password }) {
    return axios
      .post(URL + '/token', {
        email,
        password
      })
      .then(response => {
        return response.data;
      });
  }
  static auth() {
    return axios.get(URL).then(response => {
      return response.data;
    });
  }
}
