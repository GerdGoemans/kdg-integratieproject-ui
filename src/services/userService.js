import axios from 'axios';
const URL = process.env.VUE_APP_API_URL + '/users';
const TeacherURL = process.env.VUE_APP_API_URL + '/teachers';

export default class UserService {
  static async getUser() {
    return axios.get(URL + '/me').then(response => {
      return response.data;
    });
  }

  static async updateUser(userId, userObj) {
    return axios.put(URL + '/' + userId, userObj).then(response => {
      return response.data;
    });
  }

  static async createTeacher(teacherObj) {
    return axios.post(TeacherURL, teacherObj).then(response => {
      return response.data;
    });
  }
  static async deleteTeacher(teacherId) {
    return axios.delete(TeacherURL + '/' + teacherId).then(response => {
      return response.data;
    });
  }
  static async requestpw(mail) {
    return axios
      .post(URL + '/requestpassword', {
        Email: mail.toString()
      })
      .then(response => {
        return response.data;
      });
  }
  static async resetpw(resetkey) {
    return axios
      .post(URL + '/resetpassword', {
        Key: resetkey.toString()
      })
      .then(response => {
        return response.data;
      });
  }
  static async createUser(user) {
    return axios.post(URL, user).then(response => {
      return response.data;
    });
  }
}
