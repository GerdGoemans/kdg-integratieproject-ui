import axios from 'axios';

const URL = process.env.VUE_APP_API_URL + '/schools';

export default class SchoolService {
  static async getSchools(page, searchQuery) {
    return axios
      .get(URL + '?page=' + page + '&query=' + searchQuery)
      .then(response => response);
  }
  static async createSchool(school) {
    return axios.post(URL, school).then(response => response);
  }

  static async getSchoolById(schoolId) {
    return axios.get(URL + '/' + schoolId).then(response => response.data);
  }

  static async updateSchoolById(schoolId, schoolObj) {
    return axios.put(URL + '/' + schoolId, schoolObj).then(response => response.data);
  }

  static async getTeachers(page, searchQuery) {
    return axios
      .get(URL + '/teachers?page=' + page + '&query=' + searchQuery)
      .then(response => response);
  }
  static async blockSchool(schoolId, block) {
    let finalURL = URL + '/' + schoolId + '/';
    if (block) {
      finalURL += 'block';
    } else {
      finalURL += 'unblock';
    }
    return axios.patch(finalURL).then(response => response.data);
  }

  static async getLatestTeachers(schoolId) {
    return axios.get(URL + '/teachers/' + schoolId).then(response => response.data.users);
  }

  static async getLatestSchools() {
    return axios.get(URL + '/latest').then(response => response.data.schoolsDto);
  }
}
