import axios from 'axios';

const URL = process.env.VUE_APP_API_URL + '/faq';

export default class FaqService {
  static async getFaqs(page, searchQuery) {
    return axios
      .get(URL + '?page=' + page + '&query=' + searchQuery)
      .then(response => response);
  }
  static async getAllFaqs() {
    return axios.get(URL).then(response => response);
  }
  static async updateQuestion(faq) {
    return axios.patch(URL + '/' + faq.id, faq).then(response => response);
  }
  static async deleteQuestion(faq) {
    return axios.delete(URL + '/' + faq.id).then(response => response);
  }
  static async createQuestion(faq) {
    return axios.put(URL, faq).then(response => response);
  }
}
