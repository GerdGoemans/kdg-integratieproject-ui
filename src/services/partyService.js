import axios from 'axios';
const URL = process.env.VUE_APP_API_URL + '/party';

export default class PartyService {
  static async getAllParties() {
    return axios.get(URL).then(response => {
      return response.data;
    });
  }
  static async deleteParty(partyId) {
    return axios.delete(URL + '/' + partyId).then(response => {
      return response.data;
    });
  }
  static async updateParty(party) {
    return axios.patch(URL + '/' + party.id, party).then(response => {
      return response.data;
    });
  }
  static async createParty(party) {
    return axios.put(URL, party).then(response => {
      return response.data;
    });
  }
}
