import axios from 'axios';
const URL = process.env.VUE_APP_API_URL + '/Settings';

export default class SettingService {
  static async setColor(settingsId, trueOrFalseColor, color) {
    return axios
      .put(URL + '/' + settingsId + '/color/' + trueOrFalseColor, {
        Color: color
      })
      .then(response => {
        return response.data;
      });
  }

  static async getSettingById(settingsId) {
    return axios.get(URL + '/' + settingsId).then(response => {
      return response.data;
    });
  }

  static async updateSetting(settingId, setting) {
    return axios.put(URL + '/' + settingId + '/edit', setting).then(response => {
      return response.data;
    });
  }
}
