import axios from 'axios';
const URL = process.env.VUE_APP_API_URL + '/tests';

export default class TestService {
  static async createTest(test) {
    return axios.post(URL, test).then(response => {
      return response.data;
    });
  }

  static async updateTest(testId, test) {
    return axios.put(URL + '/' + testId, test).then(response => {
      return response.data;
    });
  }

  static async toggleVoteTest(testId, enabled) {
    let finalURL = `${URL}/${testId}/${enabled ? 'openVoteTest' : 'closeVoteTest'}`;
    return axios.patch(finalURL).then(response => {
      return response.data;
    });
  }

  static async deleteTest(testId) {
    return axios.delete(URL + '/' + testId).then(response => response.data);
  }

  static async markAsFavourite(testId) {
    return axios.put(URL + '/' + testId + '/favourite').then(response => response.data);
  }

  static async unfavourite(testId) {
    return axios.put(URL + '/' + testId + '/unfavourite').then(response => response.data);
  }

  static async addQuestionsToTest(testId, questions) {
    return axios.post(URL + `/${testId}/questions`, questions).then(response => {
      return response.data;
    });
  }

  static async addQuestionsToPartyTest(testId, questions) {
    return axios
      .post(URL + '/edit/' + testId + '/add/questions/partygame', questions)
      .then(response => {
        return response.data;
      });
  }
  static async addPartiesToTest(testId, parties) {
    return axios.post(URL + `/${testId}/parties`, parties).then(response => {
      return response.data;
    });
  }
  static async getTestVisibilities() {
    return axios.get(URL + '/visibilities').then(response => {
      return response.data;
    });
  }

  static async getVoteTestsAsStudent() {
    return axios.get(URL + '/votetest').then(response => response.data);
  }

  static async getTestTypes() {
    return axios.get(URL + '/types').then(resp => {
      return resp.data;
    });
  }

  static async getTags() {
    return axios.get(URL + '/tags').then(response => {
      return response.data;
    });
  }
  static async getTestById(id) {
    return axios.get(URL + '/' + id).then(response => {
      return response.data;
    });
  }

  static async getTestsByUserId(
    page,
    query,
    onlyFavourites = false,
    onlySharedTest = false
  ) {
    let finalURL = `${URL}?page=${page}`;
    if (onlyFavourites === true) {
      finalURL = `${finalURL}&onlyFavourites=true`;
    }
    if (onlySharedTest === true) {
      finalURL = `${finalURL}&onlysharedtest=true`;
    }
    finalURL = `${finalURL}&query=${query}`;
    return axios.get(finalURL).then(response => response);
  }
  static async getVoteTests(
    page,
    query,
    onlyFavourites = false,
    onlySharedTest = false,
    onlyVoteTest = true
  ) {
    let finalURL = `${URL}?page=${page}`;
    if (onlyFavourites === true) {
      finalURL = `${finalURL}&onlyFavourites=true`;
    }
    if (onlySharedTest === true) {
      finalURL = `${finalURL}&onlysharedtest=true`;
    }
    if (onlyVoteTest === true) {
      finalURL = `${finalURL}&onlyVoteTest=true`;
    }
    finalURL = `${finalURL}&query=${query}`;
    return axios.get(finalURL).then(response => response);
  }

  static async getQuestionsByTestId(id) {
    return axios.get(URL + '/' + id + '/questions').then(response => {
      return response.data;
    });
  }

  static async deleteQuestionsTests(testId, array) {
    return axios
      .delete(URL + '/' + testId + '/questions', {
        data: {
          QuestionIds: array
        }
      })
      .then(response => {
        console.log(response);
        return response.data;
      });
  }

  /**
   * Updates the questions from a test
   * @param {string} testId test id
   * @param {object} questions body object
   */
  static async updateQuestionsFromTest(testId, questions) {
    return axios.put(URL + '/' + testId + '/questions', questions).then(response => {
      return response.data;
    });
  }

  /**
   * Update the parties linked to the party test
   * @param {string} testId test id
   * @param {object} parties party id's
   */
  static async updatePartiesFromTest(testId, parties) {
    return axios
      .put(URL + `/${testId}/parties`, {
        partyIds: parties
      })
      .then(resp => resp.data);
  }

  /**
   * Updates the statements from an existing test
   * @param {string} testId test id
   * @param {object} statements statement
   */
  static async updateStatementsFromTest(testId, statements) {
    return axios
      .put(`${URL}/${testId}/parties/statements`, {
        partyAnswers: statements
      })
      .then(resp => resp.data);
  }

  /**
   * @deprecated
   */
  static async updateQuestionsFromPartyTest(testId, questions) {
    return axios
      .put(URL + '/edit/' + testId + '/update/questions/partygame', questions)
      .then(response => {
        return response.data;
      });
  }

  static async exportToCSV(testId) {
    return axios.get(URL + '/' + testId + '/csv').then(response => {
      return response.data;
    });
  }

  static async importToCSV(data) {
    return axios.post(URL + '/csv', data).then(response => {
      return response.data;
    });
  }

  static async addTagsWithTestId(testId, object) {
    return axios.put(URL + '/' + testId + '/tags', object).then(response => {
      return response.data;
    });
  }

  static async addPartyAnswersToQuestions(testId, partyAnswers) {
    return axios
      .post(URL + `/${testId}/parties/statements`, partyAnswers)
      .then(response => {
        return response.data;
      });
  }

  /**
   * @deprecated
   */
  static async getPartyAnswers(array) {
    console.log(array);

    return axios
      .get(URL + '/questions/partyanswers', {
        data: {
          AnswerIds: array
        }
      })
      .then(response => {
        console.log(response);
        return response.data;
      });
  }

  /**
   * Retrieves all parties assigned to a test.
   * @param {string} testId Test id
   */
  static async getPartiesFromTest(testId) {
    return axios.get(URL + `/${testId}/parties`).then(resp => {
      return resp.data;
    });
  }

  static async setSettingsFor(testId, settings) {
    return axios.put(URL + `/${testId}/settings`, settings).then(resp => {
      return resp.data;
    });
  }

  // updates visibility of a test
  static async updateVisibilityTest(testId, visibility) {
    return axios.put(URL + '/' + testId + '/visibility/' + visibility).then(response => {
      return response.data;
    });
  }

  /**
   * Retrieves the 8 most popular tests.
   */
  static async getPopularTests() {
    return axios.get(URL + '/popular').then(resp => {
      return resp.data;
    });
  }

  static async duplicateTest(testId) {
    return axios.get(URL + `/${testId}/duplicate`).then(resp => {
      return resp.data;
    });
  }
  static async getWordexplanationsById(testId) {
    return axios.get(URL + `/${testId}/explanations`).then(resp => {
      return resp.data;
    });
  }
}
