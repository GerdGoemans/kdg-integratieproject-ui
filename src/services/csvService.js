import axios from 'axios';
const URL = process.env.VUE_APP_API_URL + '/csv';

export default class CsvService {
  /**
   * Imports a test into the database via CSV format.
   * @param {FormData} formData
   */
  static async uploadTest(formData) {
    return axios
      .post(URL, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
      .then(resp => resp.data);
  }

  /**
   * Imports all definitions into the test with id.
   * @param {String} testId
   * @param {FormData} formData
   */
  static async uploadDefinitions(testId, formData) {
    return axios
      .post(URL + `/${testId}/explanations`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
      .then(resp => resp.data);
  }

  static async downloadTestData(testId) {
    return axios.get(URL + '/' + testId).then(r => this.forceFileDownload(r, 'data'));
  }

  static async downloadDefinitions(testId) {
    return axios
      .get(URL + `/${testId}/explanations`)
      .then(r => this.forceFileDownload(r, 'explanations'));
  }

  static forceFileDownload(response, name) {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', `${name}.csv`);
    document.body.appendChild(link);
    link.click();
  }
}
