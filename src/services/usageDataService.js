import axios from 'axios';

const URL = process.env.VUE_APP_API_URL + '/UsageData';

export default class AuthService {
  static async postRoute(fromPage, toPage, role) {
    return axios
      .post(URL + '/pagevisit', {
        FromPage: fromPage,
        ToPage: toPage,
        Role: role
      })
      .then(response => {
        return response.data;
      });
  }

  static async loadPageViews() {
    return axios.get(URL + '/pagevisists').then(response => {
      return response.data;
    });
  }
  static async loadTestUsage() {
    return axios.get(URL + '/tests').then(response => {
      return response.data;
    });
  }
  static async loadSchoolUsage() {
    return axios.get(URL + '/schools').then(response => {
      return response.data;
    });
  }
}
