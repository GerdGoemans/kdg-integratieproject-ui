export class AnswerParty {
  constructor(
    id = '0',
    partyId = '',
    Agree = false,
    explanation = '',
    questionId = '',
    answerId = ''
  ) {
    this.id = id;
    this.PartyId = partyId;
    this.Agree = Agree;
    this.Explanation = explanation;
    this.QuestionId = questionId;
    this.AnswerId = answerId;
  }
}
