export class Test {
  constructor(
    id = '',
    title = '',
    visibility = 1,
    languageCode = 'be-nl',
    type = 1
  ) {
    this.id = id;
    this.Title = title;
    this.Visibility = visibility;
    this.TestType = type;
    this.languageCode = languageCode;
  }
}
