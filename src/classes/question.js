//import { Answer } from './answer';

export class Question {
  constructor(
    id = '',
    questionString = '',
    video = '',
    languageCode = 'be-nl',
    allowNoOpinion = true,
    allowSkip = false,
    allowGiveArgument = false,
    forceGiveArgument = false,
    questionId = '',
    answers = []
  ) {
    this.Id = id;
    this.QuestionString = questionString;
    this.Video = video;
    this.LanguageCode = languageCode;
    this.AllowNoOpinion = allowNoOpinion;
    this.AllowSkip = allowSkip;
    this.AllowGiveArgument = allowGiveArgument;
    this.ForceGiveArgument = forceGiveArgument;
    this.QuestionId = questionId;
    this.PossibleAnswers = answers;
  }
}
