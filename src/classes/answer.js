export class Answer {
  constructor(
    id = '0',
    IsCorrectAnswer = false,
    Agree = false,
    Unanswered = false,
    answerId = ''
  ) {
    this.id = id;
    this.IsCorrectAnswer = IsCorrectAnswer;
    this.Agree = Agree;
    this.Unanswered = Unanswered;
    this.AnswerId = answerId;
  }
}
