export default class Party {
  constructor(id = '', name = '', slogan = '', color = '', logo = '') {
    this.Id = id;
    this.Name = name;
    this.Slogan = slogan;
    this.Color = color;
    this.Logo = logo;
  }
}
