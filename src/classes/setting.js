export default class Setting {
  constructor(
    id = '',
    testTitle = '',
    allowFreeAnswering = false,
    allowSeeScore = true,
    allowViewAnswersWhenFinished = true,
    trueColor = '#00FF00',
    falseColor = '#FF0000',
    privacy = 0,
    privacyString = '',
    testType = 0,
    testTypeString = '',
    courses = [],
    tags = []
  ) {
    this.Id = id;
    this.TestTitle = testTitle;
    this.AllowFreeAnswering = allowFreeAnswering;
    this.AllowSeeScore = allowSeeScore;
    this.AllowViewAnswersWhenFinished = allowViewAnswersWhenFinished;
    this.TrueColor = trueColor;
    this.FalseColor = falseColor;
    this.Privacy = privacy;
    this.PrivacyString = privacyString;
    this.TestType = testType;
    this.TestTypeString = testTypeString;
    this.Courses = courses;
    this.Tags = tags;
  }
}
