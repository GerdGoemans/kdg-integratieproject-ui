import Vue from 'vue';
import Vuex from 'vuex';

import axios from 'axios';
import UserService from '../services/userService';
import SchoolService from '../services/schoolService';

import VueCookies from 'vue-cookies';

Vue.use(VueCookies);
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    logged_in: Vue.$cookies.isKey('userToken'),
    token: Vue.$cookies.get('userToken') || '',
    dark: Vue.$cookies.get('dark') || 'false',
    user: {},
    school: {},
    gameSession: {},
    headerSet: false
  },
  getters: {
    isLoggedIn: state => {
      return state.logged_in;
    },
    getDark: state => {
      return state.dark;
    },
    isHeaderSet: state => {
      return state.headerSet;
    },
    getUser: state => {
      return state.user;
    },
    getSchool: state => {
      return state.school;
    },
    getToken: state => state.token,
    session: s => s.gameSession,
    isSuperAdmin: state => state.user.role == 3
  },
  mutations: {
    setSession: (s, session) => {
      s.gameSession = session;
    },
    setDark: state => {
      if (state.dark === 'true') {
        state.dark = 'false';
      } else {
        state.dark = 'true';
      }
    },
    removeDarkMode: state => {
      state.dark = 'false';
    },
    setToken: (state, token) => {
      state.token = token;
      state.logged_in = true;
      Vue.prototype.stopSignalR(true);
    },
    setUser: (state, user) => {
      state.user = user;
    },
    setSchool: (state, school) => {
      state.school = school;
    },
    headerSet: (state, value) => {
      state.headerSet = value;
    },
    logout: state => {
      state.user = {};
      state.school = {};
      state.token = undefined;
      state.logged_in = false;
      state.headerSet = false;
      Vue.$cookies.remove('userToken');
    }
  },
  actions: {
    async fetchLoginData({ commit }) {
      let user;
      try {
        user = await UserService.getUser();
      } catch (error) {
        console.log(error);
        commit('logout');
        return;
      }
      if (!user) return;
      commit('setUser', user);

      if (user.schoolId && user.role == 2) {
        const school = await SchoolService.getSchoolById(user.schoolId);
        commit('setSchool', school);
      }
    },
    toggleDarkMode: ({ state, commit }) => {
      commit('setDark');
      Vue.$cookies.set('dark', state.dark);
    },
    setToken: ({ commit }, token) => {
      commit('setToken', token);
      // save token in cookie
      Vue.$cookies.set('userToken', token);

      // set auth header
      commit('headerSet', true);
      axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    },
    setUser: ({ commit }, user) => {
      commit('setUser', user);
    },
    setSchool: ({ commit }, school) => {
      commit('setSchool', school);
    },
    headerSet: ({ commit }) => {
      commit('headerSet', true);
    },
    logout: ({ commit }) => {
      commit('logout');
      commit('removeDarkMode');
      // remove cookie with token
      Vue.$cookies.remove('userToken');
      Vue.$cookies.remove('dark');
      delete axios.defaults.headers.common['Authorization'];
    }
  },
  modules: {}
});
