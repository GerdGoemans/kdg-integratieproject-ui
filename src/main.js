import Vue from 'vue';

import App from './App.vue';
import i18n from './i18n';
import Fragment from 'vue-fragment';
import VueParticles from 'vue-particles';
import VueSimpleAlert from 'vue-simple-alert';

import VueCookies from 'vue-cookies';
import store from './store';
import router from './router';
import SessionHub from './SessionHub';
import VueToastify from 'vue-toastify';
import { BulmaAccordion, BulmaAccordionItem } from 'vue-bulma-accordion';

import './registerServiceWorker';
import './sass/reset.css';
import './sass/hint.css';
import './sass/loading.css';
import 'bulma/bulma.sass';
import './sass/global.scss';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
  faUserCircle,
  faGraduationCap,
  faSignOutAlt,
  faUser,
  faUsers,
  faEdit,
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faPlus,
  faChevronDown,
  faChevronUp,
  faChevronRight,
  faTrash,
  faShareAlt,
  faStar,
  faEnvelope,
  faLock,
  faSun,
  faMoon,
  faUpload,
  faFileDownload
} from '@fortawesome/free-solid-svg-icons';
import { faStar as farStar, faEye } from '@fortawesome/free-regular-svg-icons';

import Main from './layouts/Main.vue';
import Default from './layouts/Default.vue';

Vue.component('default-layout', Default);
Vue.component('main-layout', Main);
Vue.component('BulmaAccordion', BulmaAccordion);
Vue.component('BulmaAccordionItem', BulmaAccordionItem);

Vue.config.productionTip = false;

library.add(
  faUserCircle,
  faGraduationCap,
  faSignOutAlt,
  faUser,
  faUsers,
  faPlus,
  faEdit,
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faChevronDown,
  faChevronUp,
  faChevronRight,
  faTrash,
  faShareAlt,
  faStar,
  farStar,
  faEnvelope,
  faLock,
  faEye,
  faSun,
  faMoon,
  faUpload,
  faFileDownload
);

// setup utilities
Vue.use(Fragment.Plugin);
Vue.use(VueParticles);
Vue.use(VueSimpleAlert);
Vue.use(VueCookies);
Vue.component('fa', FontAwesomeIcon);
Vue.use(VueCookies);
Vue.use(VueToastify, {
  singular: false,
  position: 'bottom-center',
  maxToasts: 1
});
Vue.use(SessionHub);

Vue.$cookies.config('7d');

new Vue({
  store,
  i18n,
  router,
  render: h => h(App)
}).$mount('#app');
